# Spring oauth2 authorization server示例项目

#### 项目说明
Spring OAuth2 Authorization Server集成与拓展项目，包括认证服务搭建、三方登录对接、自定义grant_type方式获取token、前后端分离实现，客户端、资源服务、前端单体项目对接认证服务实现等。

#### 项目支持的授权方式
授权码模式
客户端模式
短信登录（自定义grant_type）
密码模式（自定义grant_type）

#### 项目环境要求

Java版本大于等于17

Springboot版本大于等于3.1.0-RC1

IDE安装Lombok插件
