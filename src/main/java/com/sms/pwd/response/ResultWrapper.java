package com.sms.pwd.response;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class ResultWrapper<T> implements Serializable {

    private static final String SUCCESS_MSG = "操作成功";

    private static final String ERROR_MSG = "操作失败";

    private static final int SUCCESS_CODE = 200;

    private static final int BUSINESS_ERROR_CODE = -1;

    private static final long serialVersionUID = 1L;

    private String msg;

    private int code;

    private T data;

    private long timestamp;


    public static ResultWrapper success(Object obj) {
        return builder().setData(obj).setMsg(SUCCESS_MSG).setCode(SUCCESS_CODE).setTimestamp(System.currentTimeMillis());
    }

    public static ResultWrapper success() {
        return builder().setData(null).setMsg(SUCCESS_MSG).setCode(SUCCESS_CODE).setTimestamp(System.currentTimeMillis());
    }

    public static ResultWrapper error(Object obj) {
        return builder().setData(obj).setMsg(ERROR_MSG).setCode(BUSINESS_ERROR_CODE).setTimestamp(System.currentTimeMillis());
    }

    public static ResultWrapper error(String msg) {
        return builder().setData(null).setMsg(msg).setCode(BUSINESS_ERROR_CODE).setTimestamp(System.currentTimeMillis());
    }

    public static ResultWrapper error(String msg, Integer code) {
        return builder().setData(null).setMsg(msg).setCode(code).setTimestamp(System.currentTimeMillis());
    }

    public static ResultWrapper error() {
        return builder().setData(null).setMsg(ERROR_MSG).setCode(BUSINESS_ERROR_CODE).setTimestamp(System.currentTimeMillis());
    }


    public static ResultWrapper builder() {
        return new ResultWrapper();
    }

    public ResultWrapper() {
    }
}