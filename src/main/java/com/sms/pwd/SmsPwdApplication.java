package com.sms.pwd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmsPwdApplication {

    public static void main(String[] args) {

        SpringApplication.run(SmsPwdApplication.class);

        System.out.println("application is running ......");
    }
}
